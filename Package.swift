// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MPUtils",
    platforms: [
        .iOS(.v10),
        .tvOS(.v10)
    ],
    products: [
        .library(
            name: "MPUtils",
            targets: ["MPUtils"])
    ],
    dependencies: [
        .package(
            url: "https://github.com/devicekit/DeviceKit.git",
            .upToNextMinor(from: "4.5.2")
        )
    ],
    targets: [
        .target(
            name: "MPUtils",
            dependencies: ["DeviceKit"]),
    ]
)
