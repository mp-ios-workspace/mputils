
import UIKit

public extension UIApplication {
    
    class Logger {
        
        public enum Level {
            case warning
            case error
            case info
            
            var symbol: String {
                switch self {
                case .warning:
                    return "🟧"
                case .error:
                    return "🟥"
                case .info:
                    return "🟦"
                }
            }
        }
        
        public static var isEnabled = true
        
        private init() {}
        
        public class func warning(_ message: String, function: String = #function, file: String = #file, line: Int = #line) {
            log(message, level: .warning, function: function, file: file, line: line)
        }
        
        public class func error(_ message: String, function: String = #function, file: String = #file, line: Int = #line) {
            log(message, level: .error, function: function, file: file, line: line)
        }
        
        public class func info(_ message: String, function: String = #function, file: String = #file, line: Int = #line) {
            log(message, level: .info, function: function, file: file, line: line)
        }

        private class func removeFilePath(_ file: inout String) {
            guard let indexOfLastSlash = file.lastIndex(where: { $0 == "/" }) else { return }
            let firstIndex = file.startIndex
            file.removeSubrange(firstIndex...indexOfLastSlash)
            file = file.replacingOccurrences(of: ".swift", with: "")
        }
        
        private class func log(_ message: String, level: Level, function: String = #function, file: String = #file, line: Int = #line) {
            guard isEnabled else { return }
            var fileName = file
            removeFilePath(&fileName)
            
            print("\(level.symbol) Called by \(fileName).\(function): \(message) at line \(line)")
        }
        
    }
    
}
