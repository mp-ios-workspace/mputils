
import Foundation

open class MPCodableProperty<T: Codable> {
    
    private let _userDefaults = UserDefaults.standard
    private let _key: String
    private let _defaultValue: T?
    
    public var value: T? {
        get { get() ?? _defaultValue }
        set { set(model: newValue) }
    }
    
    public var exists: Bool {
        self.get() != nil
    }
    
    public init(_ key: String) {
        _key = key
        _defaultValue = nil
    }
    
    public init(_ key: String, defaultValue: T?) {
        _key = key
        _defaultValue = defaultValue
    }
    
    public func clear() {
        value = nil
    }
    
    private func get() -> T? {
        guard let data = _userDefaults.data(forKey: _key) else { return nil }
        return try? JSONDecoder().decode(T.self, from: data)
    }
    
    private func set(model: T?) where T: Codable {
        guard let model = model else {
            _userDefaults.set(nil, forKey: _key)
            return
        }
        let data = try? JSONEncoder().encode(model)
        _userDefaults.set(data, forKey: _key)
    }
    
}
