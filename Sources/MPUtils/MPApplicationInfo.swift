
import UIKit
import DeviceKit

public enum MPApplicationInfo {
    public static var appVersion: String? {
        Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    public static var bundleIdentifier: String? {
        Bundle.main.bundleIdentifier
    }
    
    public static var deviceModel: String {
        Device.current.description
    }
    
    public static var systemVersion: String {
        UIDevice.current.systemVersion
    }
}
