
import Foundation

open class MPLocalization {
    
    public static let shared = MPLocalization()
    
    private(set) public var currentLanguage: String
    internal var currentBundle: Bundle = Bundle.main
    private let appLanguage = MPStoredProperty<[String]>("AppleLanguages")
    
    open var bundle: Bundle {
        return currentBundle
    }
    
    private init() {
        let language: String = appLanguage.value?.first ?? Locale.current.languageCode!
        currentLanguage = String(language.prefix(2))
        setCurrentBundlePath(currentLanguage)
    }
    
    open func setAppLanguage(_ languageCode: String) {
        setCurrentLanguage(languageCode)
        setCurrentBundlePath(languageCode)
    }
    
    private func setCurrentLanguage(_ languageCode: String) {
        currentLanguage = languageCode
        appLanguage.value = [languageCode]
    }
    
    private func setCurrentBundlePath(_ languageCode: String) {
        guard let url = Bundle.main.url(forResource: languageCode, withExtension: "lproj") else {
            currentBundle = .main
            return
        }
        currentBundle = Bundle(url: url) ?? .main
    }
    
}
