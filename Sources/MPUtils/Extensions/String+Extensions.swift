
import Foundation

public extension String {
    
    func localized(
        bundle: Bundle = .appBundle,
        tableName: String = "Localizable"
    ) -> String {
        
        NSLocalizedString(self, tableName: tableName, bundle: bundle, comment: "")
    }
    
}
