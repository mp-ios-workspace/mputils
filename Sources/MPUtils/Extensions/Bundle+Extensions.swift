//
//  File.swift
//  
//
//  Created by Alexander on 08.03.2022.
//

import Foundation

public extension Bundle {

    class var appBundle: Bundle {
        MPLocalization.shared.currentBundle
    }
    
    class func url(for resourceName: String, withExtension: String) -> URL? {
        appBundle.url(forResource: resourceName, withExtension: withExtension)
    }

}
