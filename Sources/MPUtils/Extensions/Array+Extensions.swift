//
//  File.swift
//  
//
//  Created by Alexander on 12.03.2022.
//

import Foundation

public extension Array {
    
    func element(_ index: Int) -> Element? {
        guard indices.contains(index) else { return nil }
        return self[index]
    }
    
}
