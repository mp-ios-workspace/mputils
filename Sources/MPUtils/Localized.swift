
import Foundation

@propertyWrapper
public struct Localized {
    private let key: String
    
    public var wrappedValue: String {
        return key.localized()
    }
    
    public init(wrappedValue: String) {
        key = wrappedValue
    }
}
