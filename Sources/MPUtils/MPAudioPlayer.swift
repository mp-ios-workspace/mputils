
import AVKit

open class MPAudioPlayer: NSObject {
    
    private let cache: NSCache<NSString, NSData>?
    private var currentTimeTimer: Timer?
    private var currentRequest: URLSessionDataTask?
    private var isCachingEnabled = true
    
    public let audioSession = AVAudioSession.sharedInstance()
    public var isLoggingEnabled = true
    
    open var player: AVAudioPlayer?
    open var playingDidFinish: (() -> Void)?
    open var readyToPlay: (() -> Void)?
    
    open var currentTimeChanged: ((TimeInterval) -> Void)? {
        didSet { handleTimer() }
    }
    
    open var audioData: Data? {
        didSet { handle(data: audioData) }
    }
    
    open var localURL: URL? {
        didSet { handleLocal(url: localURL) }
    }
    
    open var remoteURL: URL? {
        didSet { handleRemote(url: remoteURL) }
    }
    
    public init(cache: NSCache<NSString, NSData>? = nil) {
        self.cache = cache
        isCachingEnabled = cache != nil
        
        super.init()
        do {
            try audioSession.setActive(true)
            try audioSession.overrideOutputAudioPort(.speaker)
        } catch {
            UIApplication.Logger.error(error.localizedDescription)
        }
    }
    
    open func play() {
        player?.play()
    }
    
    open func pause() {
        player?.pause()
    }
    
    open func stop() {
        player?.stop()
    }
    
    open func seek(percentage: Float) {
        guard let duration = player?.duration else { return }
        player?.currentTime = duration * TimeInterval(percentage)
    }
    
    open func seek(second: Float) {
        player?.currentTime = TimeInterval(second)
    }
    
    open func seekForward(seconds: Float) {
        player?.currentTime += TimeInterval(seconds)
    }
    
    open func seekBackward(seconds: Float) {
        player?.currentTime -= TimeInterval(seconds)
    }
    
    open func download(url: URL, completion: ((Data) -> ())? = nil) {
        currentRequest?.cancel()
        currentRequest = URLSession.shared.dataTask(with: url) { [weak self] data, _, _ in
            guard let data = data else {
                if self?.isLoggingEnabled == true {
                    UIApplication.Logger.error("Downloading audio failed (data nil)")
                }
                return
            }
            self?.handle(data: data)
            completion?(data)
            
        }
        currentRequest?.resume()
    }
    
    open func handle(data: Data?) {
        guard let data = data else {
            player = nil
            return
        }
        
        do {
            player = try AVAudioPlayer(data: data)
            player?.delegate = self
            if player != nil { handleReadyToPlay() }
        } catch {
            UIApplication.Logger.error(error.localizedDescription)
        }
    }
    
    open func handleLocal(url: URL?) {
        guard let url = url else {
            player = nil
            return
        }
        
        do {
            player = try AVAudioPlayer(contentsOf: url)
            player?.delegate = self
            if player != nil { handleReadyToPlay() }
        } catch {
            UIApplication.Logger.error(error.localizedDescription)
        }
    }
    
    open func handleRemote(url: URL?) {
        guard let url = url else {
            player = nil
            return
        }
        
        guard isCachingEnabled else {
            download(url: url)
            return
        }
        
        let key = url.absoluteString as NSString
        guard let data = cache?.object(forKey: key) else {
            download(url: url) { [weak self] in
                self?.cache?.setObject($0 as NSData, forKey: key)
            }
            return
        }
        
        handle(data: data as Data)
    }
    
    open func handleTimer() {
        guard currentTimeChanged != nil else {
            currentTimeTimer = nil
            
            return
        }
        
        currentTimeTimer = .init(timeInterval: 0.1, repeats: true, block: { [weak self] _ in
            guard
                let currentTime = self?.player?.currentTime,
                self?.player?.isPlaying == true
            else {
                return
            }
            self?.currentTimeChanged?(currentTime)
        })
        RunLoop.main.add(currentTimeTimer!, forMode: .common)
    }
    
    open func handleReadyToPlay() {
        if isLoggingEnabled {
            UIApplication.Logger.info("\(player?.url?.absoluteString ?? "") ready to play")
        }
        
        player?.prepareToPlay()
        currentTimeChanged?(0)
        readyToPlay?()
    }
    
}

extension MPAudioPlayer: AVAudioPlayerDelegate {
    
    open func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playingDidFinish?()
    }
    
}
