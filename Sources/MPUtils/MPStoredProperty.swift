
import Foundation

open class MPStoredProperty<T> {
    
    private let _userDefaults = UserDefaults.standard
    private let _key: String
    private let _defaultValue: T?
    
    public var value: T? {
        get { ( _userDefaults.object(forKey: _key) as? T) ?? _defaultValue }
        set { _userDefaults.set(newValue, forKey: _key) }
    }
    
    public var exists: Bool {
        value != nil
    }
    
    public init(_ key: String) {
        _key = key
        _defaultValue = nil
    }
    
    public init(_ key: String, defaultValue: T?) {
        _key = key
        _defaultValue = defaultValue
    }
    
    public func clear() {
        value = nil
    }
    
}


